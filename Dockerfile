FROM openjdk:8-jre

LABEL maintainer="Grégory Van den Borre vandenborre.gregory@hotmail.fr"

RUN mkdir /yildiz

ENTRYPOINT ["java", "-jar", "/webapp.jar", "--brokerconfig=/yildiz/broker.properties", "--dbconfig=/yildiz/database.properties", "--port=9090"]

ADD target/webapp.jar /webapp.jar
