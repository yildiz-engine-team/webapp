/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 */

package be.yildizgames.web.webapp;

import be.yildizgames.common.file.FileProperties;
import be.yildizgames.module.database.DataBaseConnectionProvider;
import be.yildizgames.module.database.DatabaseConnectionProviderFactory;
import be.yildizgames.module.database.DbProperties;
import be.yildizgames.module.database.SimpleDbProperties;
import be.yildizgames.module.messaging.Broker;
import be.yildizgames.module.messaging.BrokerProperties;
import be.yildizgames.module.messaging.SimpleBrokerProperties;
import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.nio.file.Paths;
import java.sql.SQLException;

/**
 * @author Grégory Van den Borre
 */
@SpringBootApplication
@ComponentScan("be.yildizgames.web.webapp.infrastructure.*")
public class EntryPoint {

    @Value("${dbconfig}")
    private String databaseConfigFile;

    @Value(("${brokerconfig}"))
    private String brokerConfigFile;

    /**
     * AJP port.
     */
    @Value("${port}")
    private int port;

    @Bean
    public ServletWebServerFactory servletContainer() {

        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();

        Connector ajpConnector = new Connector("AJP/1.3");
        ajpConnector.setPort(this.port);
        ajpConnector.setSecure(false);
        ajpConnector.setAllowTrace(false);
        ajpConnector.setScheme("http");
        tomcat.addAdditionalTomcatConnectors(ajpConnector);

        return tomcat;
    }

    @Bean
    public DataBaseConnectionProvider getConnectionProvider() throws SQLException {
        DbProperties properties = new SimpleDbProperties(
                FileProperties.getPropertiesFromFile(Paths.get(this.databaseConfigFile)));
        return DatabaseConnectionProviderFactory.getInstance().create(properties);
    }

    @Bean
    public Broker getBroker() {
        BrokerProperties properties = new SimpleBrokerProperties(FileProperties.getPropertiesFromFile(Paths.get(this.brokerConfigFile)));
        return Broker.getBroker(properties);
    }

    public static void main(String[] args) {
        SpringApplication.run(EntryPoint.class, args);
    }
}
