/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2017 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.web.webapp.infrastructure.io.account;

import be.yildizgames.module.messaging.Broker;
import be.yildizgames.module.messaging.BrokerMessageDestination;
import be.yildizgames.module.messaging.Header;
import be.yildizgames.module.messaging.JmsMessageProducer;
import be.yildizgames.web.webapp.domain.account.TemporaryAccount;
import be.yildizgames.web.webapp.infrastructure.controller.AjaxResponse;
import be.yildizgames.web.webapp.infrastructure.controller.Notification;
import be.yildizgames.web.webapp.infrastructure.services.AccountCreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Grégory Van den Borre
 */
@Service
public class JmsAccountCreation implements AccountCreationService {

    private final Map <String, DeferredResult<AjaxResponse>> results = new HashMap<>();

    private final JmsMessageProducer producer;

    @Autowired
    public JmsAccountCreation(Broker broker) {
        super();
        BrokerMessageDestination responseQueue = broker.registerQueue("authentication-creation-temporary");
        responseQueue.createConsumer(message -> {
            String correlationId = message.getCorrelationId();
            Optional.ofNullable(results.get(correlationId)).ifPresent(r -> r.setResult(new AjaxResponse(List.of(
                    new Notification("test", message.getText(), "success")))));
        });
        BrokerMessageDestination requestQueue = broker.registerQueue("create-account-request");
        this.producer = requestQueue.createProducer();

    }

    @Override
    public void send(TemporaryAccount ta, DeferredResult<AjaxResponse> response) {
        String cId = UUID.randomUUID().toString();
        this.results.put(cId, response);
        producer.sendMessage(ta.getLogin() + "@@" + ta.getPassword() + "@@" + ta.getEmail(),
                Header.correlationId(cId));
    }
}
