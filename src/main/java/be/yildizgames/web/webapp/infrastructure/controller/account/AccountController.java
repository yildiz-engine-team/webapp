/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 */

package be.yildizgames.web.webapp.infrastructure.controller.account;

import be.yildizgames.web.webapp.domain.account.TemporaryAccount;
import be.yildizgames.web.webapp.infrastructure.controller.AjaxResponse;
import be.yildizgames.web.webapp.infrastructure.controller.Notification;
import be.yildizgames.web.webapp.infrastructure.services.AccountCreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

/**
 * @author Grégory Van den Borre
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class AccountController {

    private final AccountCreationService accountCreationService;

    @Autowired
    public AccountController(AccountCreationService accountCreationService) {
        super();
        this.accountCreationService = accountCreationService;
    }

    @RequestMapping(value = "api/v1/accounts/creations", method = RequestMethod.POST)
    public DeferredResult<AjaxResponse> create(@RequestBody AccountForm form) {
        DeferredResult<AjaxResponse> response = new DeferredResult<>(2000L, new AjaxResponse(List.of(
                new Notification(
                        "account.creation.timeout.title",
                        "account.creation.timeout.content",
                        "warning"))));
        accountCreationService.send(TemporaryAccount.create(
                form.getLogin(),
                form.getPassword(),
                form.getEmail()), response);

            return response;
    }

    @RequestMapping("api/v1/accounts/confirmations")
    public String confirm(@RequestParam String email, @RequestParam String token) {
       // this.temporaryAccountService.confirmAccount(email, token);
        return "Account confirmed for " + email;
    }
}
